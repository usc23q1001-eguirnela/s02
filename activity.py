name = "Ervin Joshua L. Guirnela"
age = 21
occupation = "Student"
movie = "Kung Fu Panda 2"
rating = 100.0

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating} %")

num1, num2, num3 = 20, 100, 46
print(num1 * num2)
print(num1 < num3)
print(num2 + num3)